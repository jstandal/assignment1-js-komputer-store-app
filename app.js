//Bank Elements
const balanceElement = document.getElementById('bank-balance');
const bankButtonElement = document.getElementById('bank-btn');

//Work/Pay Elements
const payElement = document.getElementById('pay-balance');
const workButtonElement = document.getElementById('work-btn');

//Loan Elements
const payLoanButtonElement = document.getElementById('pay-loan-btn');
const loanElement = document.getElementById('loan-balance');
const loanInfoElement = document.getElementById('loan-balance-info');
const getLoanButtonElement = document.getElementById('get-loan-btn');

//Computer Elements
const computerSelectElement = document.getElementById('computer-select');
const computerDescriptionElement = document.getElementById(
    'computer-description'
);
const computerSpecsElement = document.getElementById('computer-specs');
const computerPriceElement = document.getElementById('computer-price');
const computerBuyButtonElement = document.getElementById('computer-buy-btn');
const computerImageElement = document.getElementById('computer-img');

let balance = 0;
let loan = 0;
let pay = 0;
let computerPrice = 0;
let computers = [];

fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then((response) => response.json())
    .then((data) => (computers = data))
    .then((computers) => addComputersToStore(computers));

const addComputersToStore = (computers) => {
    computers.forEach((x) => addComputerToStore(x));
    handleComputerStoreChange(0); //Initiates the first computer to be displayed when page has loaded.
};

//Populate the select element with computer data.
const addComputerToStore = (computer) => {
    const computerElement = document.createElement('option');
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computerSelectElement.appendChild(computerElement);
};

//Handles the changing of computer to display the correct information.
const handleComputerStoreChange = (e) => {
    let selectedComputer;

    //Lets the function initiate with an index insted of an event to help serve the first computer to the screen on initiation.
    if (!isNaN(e)) {
        selectedComputer = computers[e];
    } else {
        selectedComputer = computers[e.target.selectedIndex];
    }
    computerDescriptionElement.innerText = selectedComputer.description;
    computerPriceElement.innerText = selectedComputer.price + ' kr';
    computerPrice = selectedComputer.price;
    computerImageElement.src =
        'https://noroff-komputer-store-api.herokuapp.com/' +
        selectedComputer.image;
    computerSpecsElement.innerText = '';
    for (const spec of selectedComputer.specs) {
        const specElement = document.createElement('li');
        specElement.appendChild(document.createTextNode(spec));
        computerSpecsElement.appendChild(specElement);
    }
};

//Handles the application for the loan
const applyForLoan = () => {
    if (loan) {
        alert(
            'Sorry! You have to pay off your current loan before applying for a new one'
        );
    } else {
        const loanAmount = parseInt(prompt('Enter desired loan amount:'));
        const maxLoanAmount = balance * 2;
        //Checks if the input is empty or not a number
        if (isNaN(loanAmount) || loanAmount === null) {
            alert(
                'Error! Loan amount has to be entered using numbers(e.g. 2000).'
            );
        }
        //Checks if the wished loan amount is higher than double the bank balance
        else if (loanAmount > maxLoanAmount) {
            alert(
                "Error! You can't apply for a loan greater than double the amount of your current bank balance."
            );
        } else {
            setBalance(balance + loanAmount);
            setLoan(loanAmount);
            payLoanButtonElement.style.display = 'inline-block';
            loanInfoElement.style.display = 'inline-block';
        }
    }
};

const generatePay = () => {
    setPay((pay += 100));
};

//Transfers the pay to the bank balance.
const transferPay = () => {
    if (loan > 0) {
        let downPayment = pay * 0.1;
        if (downPayment >= loan) {
            downPayment = loan;
        }
        setBalance(balance + (pay - downPayment));
        setLoan(loan - downPayment);
    } else {
        setBalance(balance + pay);
    }
    setPay(0);
};

//Transfers the full pay to the loan amount.
const payLoan = () => {
    if (loan > pay) {
        setLoan(loan - pay);
        setPay(0);
    } else {
        setPay(pay - loan);
        setLoan(0);
        payLoanButtonElement.style.display = 'none';
        loanInfoElement.style.display = 'none';
    }
};

//Deducts the price of the computer if possible
const buyComputer = () => {
    if (balance >= computerPrice) {
        setBalance(balance - computerPrice);
        alert('Congratulation on buying a new computer!');
    } else {
        alert(
            "Unfortunately you don't have sufficen funds to buy this computer at the moment."
        );
    }
};

//Setter functions for all balances, to change both variable values, and displayed values in the DOM
const setBalance = (amount) => {
    balance = amount;
    balanceElement.innerText = balance + ' kr';
};

const setPay = (amount) => {
    pay = amount;
    payElement.innerText = pay + ' kr';
};

const setLoan = (amount) => {
    loan = amount;
    loanElement.innerText = loan + ' kr';
};

computerBuyButtonElement.addEventListener('click', buyComputer);
computerSelectElement.addEventListener('change', handleComputerStoreChange);
workButtonElement.addEventListener('click', generatePay);
getLoanButtonElement.addEventListener('click', applyForLoan);
bankButtonElement.addEventListener('click', transferPay);
payLoanButtonElement.addEventListener('click', payLoan);
